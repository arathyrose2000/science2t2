
Construct a phylogenetic tree using the multiple sequence alignment files obtained in the previous Q. and analyze your results using Phylip. Use one of each parsimony, distance-based, and maximum likelihood methods and compare the three trees obtained (with and without bootstrapping). Submit the trees and summarize your observations.
(a)   Are the trees obtained by different methods in agreement, topology-wise?
(b)  Do you observe any difference with or without bootstrapping? What information does bootstrapping provide?
(c)   Are your inferences in agreement with those i.n Q1(b) above.



