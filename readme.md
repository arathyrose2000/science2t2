DNA

Normal: https://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=clustalo-I20200415-095143-0388-82574098-p1m
Phylib: https://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=clustalo-I20200415-095156-0155-61794057-p2m

Protein

Normal: https://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=clustalo-I20200415-095206-0092-18294865-p1m
Phylip: https://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=clustalo-I20200415-095218-0582-12185584-p2m

Percentage Identity

Proteins

| No  | Name      | SARS-CoV | MERS-CoV | PCoV   | Bat    | SARS-CoV2 |
| --- | --------- | -------- | -------- | ------ | ------ | --------- |
| 1   | MERS-CoV  | 100.00   | 32.16    | 31.54  | 31.95  | 31.85     |
| 2   | SARS-CoV  | 32.16    | 100.00   | 77.30  | 77.68  | 77.22     |
| 3   | PCoV      | 31.54    | 77.30    | 100.00 | 92.98  | 92.43     |
| 4   | Bat       | 31.95    | 77.68    | 92.98  | 100.00 | 97.71     |
| 5   | SARS-CoV2 | 31.85    | 77.22    | 92.43  | 97.71  | 100.00    |

DNA

| No  | Name       | SARS-CoV | MERS-CoV | PCoV   | Bat    | SARS-CoV2 |
| --- | ---------- | -------- | -------- | ------ | ------ | --------- |
| 1   | MERS-CoV   | 100.00   | 53.89    | 54.08  | 54.07  | 54.11     |
| 2   | SARS-CoV   | 53.89    | 100.00   | 79.03  | 79.47  | 79.65     |
| 3   | PCoV       | 54.08    | 79.03    | 100.00 | 85.26  | 85.44     |
| 4   | Bat        | 54.07    | 79.47    | 85.26  | 100.00 | 96.17     |
| 5   | SARS-CoV2_ | 54.11    | 79.65    | 85.44  | 96.17  | 100.00    |

PROTEIN DISTANCE BASED TREE

 | Name      | SARS-CoV | MERS-CoV | PCoV     | Bat      | SARS-CoV2 |
 | --------- | -------- | -------- | -------- | -------- | --------- |
 | MERS-CoV  | 0.000000 | 1.622879 | 1.674910 | 1.630291 | 1.621251  |
 | SARS-CoV  | 1.622879 | 0.000000 | 0.269943 | 0.263049 | 0.270066  |
 | PCoV      | 1.674910 | 0.269943 | 0.000000 | 0.072056 | 0.077636  |
 | Bat       | 1.630291 | 0.263049 | 0.072056 | 0.000000 | 0.022942  |
 | SARS-CoV2 | 1.621251 | 0.270066 | 0.077636 | 0.022942 | 0.000000  |

DNA DISTANCE BASED TREE

| Name      | SARS-CoV | MERS-CoV | PCoV     | Bat      | SARS-CoV2 |
| --------- | -------- | -------- | -------- | -------- | --------- |
| MERS-CoV  | 0.000000 | 0.882015 | 0.879150 | 0.879248 | 0.878270  |
| SARS-CoV  | 0.882015 | 0.000000 | 0.256425 | 0.250245 | 0.247689  |
| PCoV      | 0.879150 | 0.256425 | 0.000000 | 0.168305 | 0.166037  |
| Bat       | 0.879248 | 0.250245 | 0.168305 | 0.000000 | 0.039415  |
| SARS-CoV2 | 0.878270 | 0.247689 | 0.166037 | 0.039415 | 0.000000  |

sdd